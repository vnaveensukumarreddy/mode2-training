package com.pack.AccountWebClient;

import java.util.List;

//import org.springframework.data.repository.CrudRepository;

public interface AccountRepository  {

	public List<Account> getAllAccounts();
	public Account getAccount(String number);
	
}
