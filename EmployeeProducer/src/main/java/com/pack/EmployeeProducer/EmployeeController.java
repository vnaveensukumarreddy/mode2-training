package com.pack.EmployeeProducer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;




@Controller
public class EmployeeController {

	@RequestMapping(value="/employee", method = RequestMethod.GET)
    @HystrixCommand(fallbackMethod = "getDataFallBack")
    public Employee firstPage() {
        Employee emp = new Employee(1,"emp1","manager",3000.0);
        if(emp.getName().equalsIgnoreCase("emp1")) {
            throw new RuntimeException();
        }
        return emp;
    }
	public Employee getDataFallBack()
	{
		  Employee emp = new Employee(2,"emp1-FallBack","fall-manager",3000.0);
		return emp;
	}
	
}
