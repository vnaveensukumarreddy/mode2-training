package com.pack.SpringBootJPA.service;





import java.util.List;

import com.pack.SpringBootJPA.model.Student;


public interface StudentService {
	
public void saveStudent(Student student);

public List<Student> getStudentsByPage(int pageId, int total);

public long count();

public void deleteAllStudents();

public Student getStudentById(int stuId);

public void updateStudent(Student student);

public void deleteStudent(Integer stuId);
}
