package com.pack.SpringBootJPA.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.SpringBootJPA.dao.StudentDao;
import com.pack.SpringBootJPA.model.Student;
@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentDao studentDao;
	@Override
	public void saveStudent(Student student) {
		
		studentDao.save(student);

	}
	@Override
	public List<Student> getStudentsByPage(int pageId, int total) {
		List<Student> stuList=studentDao.getStudentsByPage(pageId, total);
		return stuList;
	}
	@Override
	public long count() {
		long count=studentDao.count(); 
		return count;
	}
	@Override
	public void deleteAllStudents() {
	
		studentDao.deleteAll();
		
		
	}
	@Override
	public Student getStudentById(int stuId) {
	
		Optional<Student> optional=studentDao.findById(stuId);
		Student student=null;
		if(optional.isPresent())
		{
			student=optional.get();
			System.out.println(student);
		}
		else
		{
			System.out.printf("No Student Found With Id %d%n",stuId);
		}
		return student;
	}
	@Override
	public void updateStudent(Student student) {
		
		studentDao.save(student);
	}
	@Override
	public void deleteStudent(Integer stuId) {
		studentDao.deleteById(stuId); 
		
	}
	
	

}
