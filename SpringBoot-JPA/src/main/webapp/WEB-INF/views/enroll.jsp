<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Registration Form</title>
<link
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	rel="stylesheet">
	<style>
	.has-error
	{
	color:red;
	}
	</style>
</head> 
<body>
	<div class="container-fluid">
		<h2>Student Enrollment details</h2>
		<form:form action="save" method="post" class="form-horizontal"
            modelAttribute="student" novalidate="novalidate">
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="firstName">Firstname</label>
                    <div class="col-md-7">
                        <form:input type="text" path="firstName" id="firstName"
                            class="form-control input-sm" />
                        <div class="has-error">
                            <form:errors path="firstName" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="lastName">lastName</label>
                    <div class="col-md-7">
                        <form:input type="text" path="lastName" id="lastName"
                            class="form-control input-sm" />
                        <div class="has-error">
                            <form:errors path="lastName" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="email">email</label>
                    <div class="col-md-7">
                        <form:input type="email" path="email" id="email"
                            class="form-control input-sm" />
                        <div class="has-error">
                            <form:errors path="email" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="dob">Date Of Birth</label>
                    <div class="col-md-7">
                        <form:input type="date" path="dob" id="dob"
                            class="form-control input-sm" />
                        <div class="has-error">
                            <form:errors path="dob" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="section">section</label>
                    <div class="col-md-7" class="form-control input-sm">
                        <form:radiobuttons path="section" items="${sections}" id="section" />
                        <div class="has-error">
                            <form:errors path="section" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="country">country</label>
                    <div class="col-md-7" class="form-control input-sm">
                        <form:select path="country" id="country">
                            <form:option value="">Select Country</form:option>
                            <form:options items="${countries}" />
                        </form:select>
                        <div class="has-error">
                            <form:errors path="country" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="subjects">subjects</label>
                    <div class="col-md-7">
                        <form:select path="subjects" id="subjects" items="${subjects}"
                            class="form-control input-sm">
                        </form:select>
                        <div class="has-error">
                            <form:errors path="subjects" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-table" for="sex">sex</label>
                    <div class="col-md-7" class="form-control input-sm">
                        <form:radiobutton path="sex" value="M" id="sex" />
                        Male
                        <form:radiobutton path="sex" value="F" id="sex" />
                        Female
                        <div class="has-error">
                            <form:errors path="sex" class="help-inline"/>
                    </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-table" for="firstAttempt">firstAttempt</label>
					<div class="col-md-1">
						<form:checkbox path="firstAttempt" class="form-control input-sm"
							id="firstAttempt" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-actions float-right">
					<input type="submit" value="register"
						class="btn btn-primary">
				</div>
			</div>

		</form:form>
	</div>
</body>
</html>