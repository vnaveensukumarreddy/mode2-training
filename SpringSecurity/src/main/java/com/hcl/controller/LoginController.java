package com.hcl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView welcomePage()
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("welcomePage");
		return modelAndView;
		
	}
	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
    public ModelAndView loginPage(@RequestParam(value = "error",required = false) String error,
    @RequestParam(value = "logout",    required = false) String logout) {
        System.out.println("error"+error+ "logout"+logout);
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid Credentials provided.");
            model.setViewName("loginPage");
            return model;
        }

 

        else if (logout != null) {
            model.addObject("message", "Logged out successfully.");
            model.setViewName("loginPage");
            return model;
        }
        
            return new ModelAndView("loginPage");
        
    }

 
	
}
