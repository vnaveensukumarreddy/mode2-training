package com.hcl.controller;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class LoginSecurityIntializer extends AbstractSecurityWebApplicationInitializer {

	public LoginSecurityIntializer()
	{
		super(LoginApplicationConfig.class);
	}
}
