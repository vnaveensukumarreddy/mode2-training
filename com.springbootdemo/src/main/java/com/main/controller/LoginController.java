package com.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LoginController {
    @GetMapping("/login")
    public ModelAndView login()
    {
        return new ModelAndView("login");
    }
    @GetMapping("/success")
    public ModelAndView success()
    {
        return new ModelAndView("success");
    }
    @GetMapping("/register")
    public ModelAndView register()
    {
        return new ModelAndView("register");
    }
    @GetMapping("/welcome")
    public ModelAndView welcome()
    {
        return new ModelAndView("welcome");
    }
}
 
